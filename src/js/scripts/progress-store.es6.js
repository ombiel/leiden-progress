const SimpleStore = require('-aek/simple-store');
const request = require('-aek/request');
const moment = require('moment');
const _ = require('-aek/utils');
const Notifier = require('-aek/notifier');
const { getToken } = require("-aek/client-tools/oauth");

const Config = require('../modules/config');

// these can be disabled by commenting them out of the plugins array in the constructor()
const loggerPlugin = require('-aek/simple-store/plugins/logger');
const localStoragePlugin = require('-aek/simple-store/plugins/localstorage');

// i.e. 'aek2-test-project-key', this is defined as a JavaScript string explicitly in the JSON Config object
const storageKey = Config.storageKey;
const allowRenew = _.isNative();

// this project comes with an implementation of SimpleStore, but this isn't necessary to manage state in React; it's just what we recommend
// https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.3.1/pages/other-tools/simple-store
// NOTE - with localStoragePlugin enabled, any changes to initialState will only be reflected by clearing your browser's localStorage (or by logging out and back in)
class ProgressStore extends SimpleStore {
  constructor() {
    let plugins = [];
    // comment out either of these to deactivate them
    plugins.push(loggerPlugin());
    plugins.push(localStoragePlugin(storageKey));

    super({
      initialState: {
        user: {
          currentPlanID: false
        },
        plans: {
          planData: false,
          planError: false,
          $$$loading: false,
          $$$backgroundLoading: false,
          lastUpdated: null
        },
        courses: {
          courseData: false,
          courseError: false,
          $$$loading: false,
          $$$backgroundLoading: false,
          lastUpdated: null
        }
      },
      plugins: plugins
    });

    // you can modify state values here if you absolutely have to, i.e. if you're checking for something on startup
  }

  handleErrorType(e) {
    if(e.type) {
      switch(e.type) {
        case 'WARNING':
          console.warn(e); // eslint-disable-line no-console
          break;
        case 'ERROR':
          console.error(e); // eslint-disable-line no-console
          break;
        default:
          console.warn('Unsupported error TYPE "' + e.type + '" in callback.'); // eslint-disable-line no-console
          break;
      }
    }
  }

  returnTokenPromise(serviceName) {
    return new Promise((resolve, reject) => {
      getToken({ serviceName:serviceName, allowRenew })
      .then((res) => {
        // console.warn('TOKEN :: ');
        // console.log(res);
        resolve(res);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  // you'll need one dispatch and one fetch method for each service, or group of service calls (depending on implementation)
  dispatchEducationalPlans(operation, storePath, results, error, loading, backgroundLoading = false) {
    // "group" is just a label for the development environment logger, path is important as it relates to the SimpleStore object keyname
    let ctx = this.context({ group:storePath.toUpperCase(), path:storePath });
    // moment doesn't serialise well, so store the formatted string which we can use to create new moment instances out of, should we need to
    let hasError = error !== false;
    let now = moment().format();
    ctx.dispatch({
      name: operation + '-' + storePath,
      error: hasError,
      extend:{
        planData: results,
        planError: error,
        $$$loading: loading,
        $$$backgroundLoading: backgroundLoading,
        lastUpdated: now
      }
    });
  }

  returnPlansPromise(storePath, storeData, accessToken) {
    return new Promise((resolve, reject) => {
      // console.warn('CALLING :: fetchEducationalPlans');
      request.action('get-plans').send({ accessToken:accessToken }).end((err, res) => {

        // console.warn('CALLED :: fetchEducationalPlans');
        // console.log(res);

        let existingData = this.get(storePath + '.' + storeData);
        if(!err && _.has(res, 'body.response') && res.text.indexOf('{}') !== 0) {
          let parsedResponse = JSON.parse(res.body.response);

          // console.warn('parsed PLANS response :: ')
          // console.log(parsedResponse);

          if(_.has(parsedResponse, '_embedded.items')) {
            resolve({ type:'SET', data:parsedResponse });
          } else {
            // this error can be anything informative, or even set in the Config object
            let error = 'Was expecting an items array in the service response, none found!';
            if(parsedResponse.code && parsedResponse.moreInfo) {
              error = 'HTTP ' + parsedResponse.code + ': ' + parsedResponse.moreInfo;
            }

            reject({ type:'WARNING', data:existingData, error:error });
          }
        } else {
          // we always guard object lookups with lodash checks; res.body could fail if res is undefined or null
          let error = _.has(res, 'body') ? res.body : 'Unknown response from server.';
          // only keep the Notifier for when something has gone seriously wrong
          this.genericNotifier('Server Failure', error, true, 'error');
          reject({ type:'ERROR', data:existingData, error:error });
        }
      });
    });
  }

  // an example of a default parameter value - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters
  fetchEducationalPlans(backgroundLoading = false) {
    const storePath = 'plans';
    const storeData = 'planData';

    // get any existing data from the store for if the service call fails
    let existingData = this.get(storePath + '.' + storeData);
    this.dispatchEducationalPlans('GET', storePath, existingData, false, true, backgroundLoading);

    this.returnTokenPromise('PROGRESS_INFO_EDUCATIONAL_PLANS').then((token) => {
      let plansPromise = this.returnPlansPromise(storePath, storeData, token.accessToken);

      // console.warn('TOKEN FOR PLANS :: ');
      // console.log(token);

      plansPromise.then((res) => {

        console.warn('PARSED :: fetchEducationalPlans');
        console.log(res.data);

        this.dispatchEducationalPlans(res.type, storePath, res.data, false, false);
      }).catch((e) => {
        this.dispatchEducationalPlans(e.type, storePath, e.data, e.error, false);
        this.handleErrorType(e);
      });
    }).catch((e) => {
      // console.warn(e);
      existingData = this.get(storePath + '.' + storeData);
      this.dispatchEducationalPlans('ERROR', storePath, existingData, e, false);
    });
  }

  // you'll need one dispatch and one fetch method for each service, or group of service calls (depending on implementation)
  dispatchCourses(operation, storePath, results, error, loading, backgroundLoading = false) {
    // "group" is just a label for the development environment logger, path is important as it relates to the SimpleStore object keyname
    let ctx = this.context({ group:storePath.toUpperCase(), path:storePath });
    // moment doesn't serialise well, so store the formatted string which we can use to create new moment instances out of, should we need to
    let hasError = error !== false;
    let now = moment().format();
    ctx.dispatch({
      name: operation + '-' + storePath,
      error: hasError,
      extend:{
        courseData: results,
        courseError: error,
        $$$loading: loading,
        $$$backgroundLoading: backgroundLoading,
        lastUpdated: now
      }
    });
  }

  returnCoursesPromise(storePath, storeData, accessToken, educationalPlanID) {
    return new Promise((resolve, reject) => {
      // console.warn('CALLING :: fetchCourses');
      // console.log('accessToken :: ' + accessToken);
      request.action('get-courses').send({ accessToken:accessToken, educationalPlanID:educationalPlanID }).end((err, res) => {

        // console.warn('CALLED :: fetchCourses');
        // console.log(res);

        let existingData = this.get(storePath + '.' + storeData);
        if(!err && _.has(res, 'body.response') && res.text.indexOf('{}') !== 0) {
          let parsedResponse = JSON.parse(res.body.response);
          // if the response has a different top-level attribute, or multiple attributes, update as necessary
          if(_.has(parsedResponse, '_embedded.items')) {

            console.warn('PARSED :: fetchCourses');
            console.log(parsedResponse);

            resolve({ type:'SET', data:parsedResponse });
          } else {
            // this error can be anything informative, or even set in the Config object
            let error = 'Was expecting an items array in the service response, none found!';
            if(parsedResponse.code && parsedResponse.moreInfo) {
              error = 'HTTP ' + parsedResponse.code + ': ' + parsedResponse.moreInfo;
            }

            reject({ type:'WARNING', data:existingData, error:error });
          }
        } else {
          // we always guard object lookups with lodash checks; res.body could fail if res is undefined or null
          let error = _.has(res, 'body') ? res.body : 'Unknown response from server.';
          // only keep the Notifier for when something has gone seriously wrong
          this.genericNotifier('Server Failure', error, true, 'error');
          reject({ type:'ERROR', data:existingData, error:error });
        }
      });
    });
  }

  // an example of a default parameter value - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters
  fetchCourses(educationalPlanID, backgroundLoading = false) {
    const storePath = 'courses';
    const storeData = 'courseData';

    // get any existing data from the store for if the service call fails
    let existingData = this.get(storePath + '.' + storeData);
    this.dispatchCourses('GET', storePath, existingData, false, true, backgroundLoading);

    this.returnTokenPromise('PROGRESS_INFO_COURSES').then((token) => {
      // console.warn('TOKEN FOR COURSES :: ');
      // console.log(token);
      let coursesPromise = this.returnCoursesPromise(storePath, storeData, token.accessToken, educationalPlanID);
      coursesPromise.then((res) => {
        // console.log(res);
        this.dispatchCourses(res.type, storePath, res.data, false, false);
      }).catch((e) => {
        this.dispatchCourses(e.type, storePath, e.data, e.error, false);
        this.handleErrorType(e);
      });
    }).catch((e) => {
      // console.warn(e);
      existingData = this.get(storePath + '.' + storeData);
      this.dispatchCourses('ERROR', storePath, existingData, e, false);
    });
  }

  // message takes plaintext only; HTML will be rendered as such
  // more examples of default parameters values
  genericNotifier(title, message, dismissable, level, autoDismiss = 10, clear = true) {
    if(clear) {
      Notifier.clear();
    }

    Notifier.add({
      title: title,
      message: message,
      dismissable: dismissable,
      level: level,
      autoDismiss: autoDismiss
    });
  }

  // children allows you to pass HTML to the Notifier to be displayed
  notifierWithChildren(title, children, dismissable, level, autoDismiss = 10, clear = true) {
    if(clear) {
      Notifier.clear();
    }

    Notifier.add({
      title: title,
      children: children,
      dismissable: dismissable,
      level: level,
      autoDismiss: autoDismiss
    });
  }

}

module.exports = new ProgressStore();
