const React = window.React = require('react');
const Page = require('-components/page');
const { BasicSegment } = require('-components/segment');
const { Button } = require('-components/button-group');
const { InfoMessage } = require('-components/message');
const Notifier = require('-aek/notifier');

// this is our Lodash library, with some additional functionality. See https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.3.1/pages/other-tools/utils
const _ = require('-aek/utils');

const ProgressStore = require('../scripts/progress-store');
const Config = require('../modules/config');

let IndexPage = React.createClass({
  componentDidMount:function() {
    let planId = this.props.ctx.params.planId;
    ProgressStore.fetchCourses(planId);
  },

  componentWillUnmount:function() {
    Notifier.clear();
  },

  backToPage:function(href, e) {
    e.preventDefault();
    this.props.ctx.router.backTo(href);
  },

  render:function() {
    let data = ProgressStore.get('plans.planData');

    let loading = ProgressStore.get('courses.$$$loading');
    let backgroundLoading = ProgressStore.get('courses.$$$backgroundLoading');

    // NOTE - recommended reading - https://eslint.org/docs/rules/no-console
    // console.log(data); // eslint-disable-line no-console

    let planView = (!loading || backgroundLoading) ?
      <BasicSegment>
        <InfoMessage className='message-fix'>{ Config.language.index.noPlanDataFound }</InfoMessage>
      </BasicSegment> : null;

    let planId = this.props.ctx.params.planId;
    // as we're initialising the data object as an array in our ProgressStore, we can assume that and always check for its length
    // we also need to not display any existing view behind the loading state, if loading - it often pushes the loading widget off of the viewable page
    if(_.has(data, '_embedded.items') && (!loading || backgroundLoading)) {
      let items = _.asArray(_.get(data, '_embedded.items'));
      let selectedItem = _.find(items, (item) => item.educationalPlanId === planId);
      if(selectedItem) {
        // console.log(selectedItem);
        let planName = selectedItem.name ? selectedItem.name : Config.language.index.noPlanNameFound;
        let unitCompleted;
        if(_.has(selectedItem, 'earnedEcts') && _.has(selectedItem, 'openEcts')) {
          // just to ensure 2DPI precision on the resulting %-based CSS
          let earnedWidth = (parseFloat(selectedItem.earnedEcts.toString()) / parseFloat(selectedItem.ects.toString()) * 100).toFixed(2) + '%';
          let openWidth = (parseFloat(selectedItem.openEcts.toString()) / parseFloat(selectedItem.ects.toString()) * 100).toFixed(2) + '%';
          unitCompleted =
            <div>
              <div className={ 'progress-container' }>
                <div className={ 'earned' } style={{ width:earnedWidth }} />
                <div className={ 'open' }  style={{ width:openWidth }} />
              </div>
              <div className={ 'progress-container' }>
                <div className={ 'text-earned' }>
                  { selectedItem.earnedEcts + Config.language.index.unitsTakenSuffix }
                </div>
                <div className={ 'text-open' }>
                  { selectedItem.openEcts + Config.language.index.unitsRequiredSuffix }
                </div>
              </div>
            </div>;
        }

        planView =
          <BasicSegment>
            <div>
              <h1 className={ 'index-header' }>{ planName }</h1>
            </div>
            <div>
              { unitCompleted }
            </div>
          </BasicSegment>;
      }
    }

    let backSegment = !loading || backgroundLoading ?
      <BasicSegment nopadding style={{ position:'absolute', bottom:'0', width:'100%' }}>
        <Button fluid onClick={ this.backToPage.bind(this, '/') } style={{ borderRadius:'0' }}>Back to Plans</Button>
      </BasicSegment> : null;

    return (
      <Page>
        <BasicSegment nopadding loading={ loading && !backgroundLoading } style={{ height:'100%' }}>
          { planView }
          { backSegment }
        </BasicSegment>
      </Page>
    );
  }
});

module.exports = IndexPage;
