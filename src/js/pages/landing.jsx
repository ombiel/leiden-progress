const React = window.React = require('react');
const Page = require('-components/page');
const { Panel, VBox } = require('-components/layout');
const { BasicSegment } = require('-components/segment');
const { BannerHeader } = require('-components/header');
const { InfoMessage } = require('-components/message');
const { Listview, Item } = require('-components/listview');
const Notifier = require('-aek/notifier');

// this is our Lodash library, with some additional functionality. See https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.3.1/pages/other-tools/utils
const _ = require('-aek/utils');

const ProgressStore = require('../scripts/progress-store');
const Config = require('../modules/config');

let LandingPage = React.createClass({
  componentDidMount:function() {
    ProgressStore.fetchEducationalPlans();
  },

  componentWillUnmount:function() {
    Notifier.clear();
  },

  gotoPlanPage(planId, e) {
    e.preventDefault();
    ProgressStore.set('user.currentPlanID', planId);
    let href = '/plans/' + planId;
    this.props.ctx.router.goto(href);
  },

  render:function() {
    let theme = Config.theme.primary;

    let data = ProgressStore.get('plans.planData');
    let loading = ProgressStore.get('plans.$$$loading');
    // this project currently isn't using this, but we use this as a visual switch so that we can apply different loading UI logic when data is loading silently in the background
    let backgroundLoading = ProgressStore.get('plans.$$$backgroundLoading');

    // NOTE - recommended reading - https://eslint.org/docs/rules/no-console
    // console.log(data); // eslint-disable-line no-console

    let planView = (!loading || backgroundLoading) ?
      <BasicSegment>
        <InfoMessage className='message-fix'>{ Config.language.index.noPlanDataFound }</InfoMessage>
      </BasicSegment> : null;

    // as we're initialising the data object as an array in our ProgressStore, we can assume that and always check for its length
    // we also need to not display any existing view behind the loading state, if loading - it often pushes the loading widget off of the viewable page
    if(_.has(data, '_embedded.items') && (!loading || backgroundLoading)) {
      let items = _.asArray(_.get(data, '_embedded.items'));

      let planViewItems = [];
      _.forEach(items, (item, itemIndex) => {
        let planName = item.name && item.name.length > 0 ? item.name : Config.language.index.noPlanNameFound;
        planViewItems.push(
          <Item key={ 'plan-' + itemIndex } onClick={ this.gotoPlanPage.bind(this, item.educationalPlanId) }>
            { planName }
          </Item>
        );
      });

      if(planViewItems.length > 0) {
        planView =
          <Listview className='fixed-links fixed-width' style={{ borderRadius:'0', marginTop:'1rem' }}>
            { planViewItems }
          </Listview>;
      }
    }

    return (
      <Page>
        <VBox>
          <BannerHeader theme={ theme } level='3' key='index-header' data-flex={0}>{ Config.language.landing.pageHeader }</BannerHeader>
          <Panel>
            <BasicSegment nopadding loading={ loading && !backgroundLoading }>
              { planView }
            </BasicSegment>
          </Panel>
        </VBox>
      </Page>
    );
  }
});

module.exports = LandingPage;
