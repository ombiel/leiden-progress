const React = window.React = require('react');
const Page = require('-components/page');
const { Panel, VBox } = require('-components/layout');
const { BannerHeader } = require('-components/header');
const { BasicSegment, Segment } = require('-components/segment');
const { Listview, Item } = require('-components/listview');
const { InfoMessage } = require('-components/message');
const Notifier = require('-aek/notifier');

// this is our Lodash library, with some additional functionality. See https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.3.1/pages/other-tools/utils
const _ = require('-aek/utils');

const ProgressStore = require('../scripts/progress-store');
const Config = require('../modules/config');

let CoursePage = React.createClass({
  componentWillUnmount:function() {
    Notifier.clear();
  },

  render:function() {
    let theme = Config.theme.primary;
    let altTheme = Config.theme.secondary;

    let courseType = this.props.ctx.params.type;
    let courseId = this.props.ctx.params.courseId;

    let courseData = ProgressStore.get('courses.courseData');
    let courseLoading = ProgressStore.get('courses.$$$loading');
    let courseBackgroundLoading = ProgressStore.get('courses.$$$backgroundLoading');
    let courseUILoading = courseLoading && !courseBackgroundLoading;

    // NOTE - recommended reading - https://eslint.org/docs/rules/no-console
    // console.log(courseData); // eslint-disable-line no-console

    courseData = _.asArray(_.get(courseData, '_embedded.items'));
    let selectedCourse = _.find(courseData, (course) => course.courseId === courseId);
    console.warn('selected course for courseId ' + courseId + ' :: ');
    console.log(selectedCourse);

    let courseViewBlock = !courseUILoading ?
      <BasicSegment>
        <InfoMessage>
          { Config.language.courses.noContentFoundForCourse }
        </InfoMessage>
      </BasicSegment> : null;

    if(selectedCourse && !courseUILoading) {
      let courseViewItems = [];

      if(selectedCourse.courseCatalogueId) {
        courseViewItems.push(
          <Item basicLabel labelVariation={ theme } label={ Config.language.courses.courseCatalogueIDLabel } key={ 'catalogue-id' }>
            { selectedCourse.courseCatalogueId }
          </Item>
        );
      }

      if(selectedCourse.ects) {
        courseViewItems.push(
          <Item basicLabel labelVariation={ theme } label={ Config.language.courses.unitsLabel } key={ 'course-units' }>
            { selectedCourse.ects }
          </Item>
        );
      }

      if(courseViewItems.length > 0) {
        courseViewBlock =
          <Listview uniformLabels className={ 'fixed-links' } style={{ borderRadius:'0', marginTop:'1rem' }}>
            { courseViewItems }
          </Listview>;
      }
    }

    let pageName = selectedCourse && selectedCourse.name ? selectedCourse.name : Config.language.courses.noCourseNameGiven;
    return (
      <Page>
        <VBox>
          <BannerHeader theme={ theme } level='3' key='index-header' data-flex={0}>{ pageName }</BannerHeader>
          <Panel>
            <BasicSegment nopadding loading={ courseUILoading }>
              { courseViewBlock }
            </BasicSegment>
          </Panel>
        </VBox>
      </Page>
    );
  }
});

module.exports = CoursePage;
