const React = window.React = require('react');
const Page = require('-components/page');
const { Panel, VBox } = require('-components/layout');
const { BannerHeader } = require('-components/header');
const { BasicSegment } = require('-components/segment');
const { Listview, Item } = require('-components/listview');
const { InfoMessage } = require('-components/message');
const Notifier = require('-aek/notifier');

// this is our Lodash library, with some additional functionality. See https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.3.1/pages/other-tools/utils
const _ = require('-aek/utils');

const ProgressStore = require('../scripts/progress-store');
const Config = require('../modules/config');

let CourseListPage = React.createClass({
  componentWillUnmount:function() {
    Notifier.clear();
  },

  gotoPage:function(href, e) {
    e.preventDefault();
    // console.warn('going to href :: ');
    // console.log(href);
    this.props.ctx.router.goto(href);
  },

  render:function() {
    let theme = Config.theme.primary;
    let altTheme = Config.theme.secondary;

    // this controls the type of course displayed on the page
    let courseType = this.props.ctx.params.type;
    let planId = this.props.ctx.params.planId;
    // console.warn('TYPE :: ' + courseType);

    let planData = ProgressStore.get('plans.planData');
    let planLoading = ProgressStore.get('plans.$$$loading');
    let planBackgroundLoading = ProgressStore.get('plans.$$$backgroundLoading');
    let planUILoading = planLoading && !planBackgroundLoading;

    let courseData = ProgressStore.get('courses.courseData');
    let courseLoading = ProgressStore.get('courses.$$$loading');
    let courseBackgroundLoading = ProgressStore.get('courses.$$$backgroundLoading');
    let courseUILoading = courseLoading && !courseBackgroundLoading;

    let pageName;
    let fallbackContentText;
    switch(courseType) {
      case 'enrolled':
        pageName = Config.language.courses.enrolledPageName;
        fallbackContentText = Config.language.courses.noEnrolledCoursesFound;
        break;
      case 'open':
        pageName = Config.language.courses.openPageName;
        fallbackContentText = Config.language.courses.noOpenCoursesFound;
        break;
      case 'extra':
        pageName = Config.language.courses.extraPageName;
        fallbackContentText = Config.language.courses.noExtraCoursesFound;
        break;
      default:
        // we don't want a fallback; we want the developer to know there's an issue
        break;
    }

    // NOTE - recommended reading - https://eslint.org/docs/rules/no-console
    // console.log(planData); // eslint-disable-line no-console
    // console.log(courseData); // eslint-disable-line no-console

    let courseViewBlock = !courseUILoading ?
      <BasicSegment>
        <InfoMessage>
          { fallbackContentText }
        </InfoMessage>
      </BasicSegment> : null;

    if(courseData && !courseUILoading) {
      let courseViewItems = [];

      let courseItems = _.asArray(_.get(courseData, '_embedded.items'));
      // filter specifically based on the courseType
      switch(courseType) {
        case 'enrolled':
          courseItems = _.filter(courseItems, (item) => item.status === Config.courses.validEnrolledStatus);
          break;
        case 'open':
          courseItems = _.filter(courseItems, (item) => item.status === Config.courses.validOpenStatus);
          break;
        case 'extra':
          courseItems = _.filter(courseItems, (item) => item.status === Config.courses.validExtraStatus);
          break;
        default:
          break;
      }

      if(courseItems.length > 0) {
        _.forEach(courseItems, (item, itemIndex) => {
          let name = item.name ? item.name : Config.language.courses.noCourseNameGiven;
          let onClick = this.gotoPage.bind(this, '/plans/' + planId + '/courses/' + courseType + '/course/' + item.courseId);
          courseViewItems.push(
            <Item key={ 'course-' + itemIndex } heading={ name } onClick={ onClick } />
          );
        });

        courseViewBlock =
          <Listview className={ 'fixed-links' } style={{ borderRadius:'0', marginTop:'1rem' }}>
            { courseViewItems }
          </Listview>;
      }
    }

    return (
      <Page>
        <VBox>
          <BannerHeader theme={ theme } level='3' key='course-header' data-flex={0}>{ pageName }</BannerHeader>
          <Panel>
            <BasicSegment nopadding loading={ courseUILoading }>
              { courseViewBlock }
            </BasicSegment>
          </Panel>
        </VBox>
      </Page>
    );
  }
});

module.exports = CourseListPage;
