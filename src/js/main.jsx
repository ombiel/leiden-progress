const React = window.React = require('react');
const reactRender = require('-aek/react/utils/react-render');
const { Panel, VBox, CBox } = require('-components/layout');
const { AekReactRouter, RouterView } = require('-components/router');
const Container = require('-components/container');

const _ = require('-aek/utils');
const router = new AekReactRouter();

const LandingPage = require('./pages/landing');
const IndexPage = require('./pages/index');
const CourseListPage = require('./pages/course-list');
const CoursePage = require('./pages/course-page');

const ProgressStore = require('./scripts/progress-store');
const Config = require('./modules/config');

let Screen = React.createClass({
  componentWillMount:function() {
    // this is required to trigger a rerender on SimpleStore update
    // only works thanks to React's fast virtual DOM; would impact performance otherwise
    ProgressStore.on('change', () => {
      this.forceUpdate();
    });
  },

  componentDidMount:function() {
    // we use this to trigger a forceUpdate on router paging, allowing us to capture currentPath
    router.addRoute("*", (ctx, next) => {
      next();
      this.forceUpdate();
    });

    // endsWith fallback for IE - https://caniuse.com/#search=endswith
    // we also have a parallel implementation in ProgressUtils if you want to avoid this
    if(!String.prototype.endsWith) {
      String.prototype.endsWith = function(suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
      };
    }
  },

  componentWillUnmount:function() {
    ProgressStore.off('change');
  },

  routeToPage:function(href, e) {
    e.preventDefault();
    router.backTo(href);
  },

  render:function() {
    let theme = Config.theme.primary;
    let altTheme = Config.theme.secondary;

    let { currentPlanID } = ProgressStore.get('user');

    let { overviewEnabled, enrolledEnabled, openEnabled, extraEnabled } = Config.navMenu;
    let { navMenuOptions, navMenuIcons } = Config.navMenu;
    let path = router.getCurrentPath();

    let planLoading = ProgressStore.get('plans.$$$loading');
    let planBackgroundLoading = ProgressStore.get('plans.$$$backgroundLoading');
    let planNavBarLoading = planLoading && !planBackgroundLoading;

    let courseLoading = ProgressStore.get('plans.$$$loading');
    let courseBackgroundLoading = ProgressStore.get('plans.$$$backgroundLoading');
    let courseNavBarLoading = courseLoading && !courseBackgroundLoading;

    // console.warn('CURRENT PLAN ID :: ');
    // console.log(currentPlanID);
    let overviewPath = '/plans/' + currentPlanID;
    let enrolledPath = '/plans/' + currentPlanID + '/courses/enrolled';
    let openPath = '/plans/' + currentPlanID + '/courses/open';
    let extraPath = '/plans/' + currentPlanID + '/courses/extra';

    let overviewClick = this.routeToPage.bind(this, overviewPath);
    let enrolledClick = !courseNavBarLoading ? this.routeToPage.bind(this, enrolledPath) : null;
    let openClick = !courseNavBarLoading ? this.routeToPage.bind(this, openPath) : null;
    let extraClick = !courseNavBarLoading ? this.routeToPage.bind(this, extraPath) : null;

    let overviewClasses = '';
    let enrolledClasses = '';
    let openClasses = '';
    let extraClasses = '';
    if(courseNavBarLoading) {
      enrolledClasses += ' disabled';
      openClasses += ' disabled';
      extraClasses += ' disabled';
    }

    // TODO :: or if course page matches
    // will require a bit of thought to develop something that works independently of router path design
    // probably why Rich's original component didn't in the first place?
    if(path.indexOf(overviewPath) === 0 && path.length === overviewPath.length) {
      overviewClasses = ' active';
    } else if(path.indexOf(enrolledPath) === 0 && path.length === enrolledPath.length) {
      enrolledClasses = ' active';
    } else if(path.indexOf(openPath) === 0 && path.length === openPath.length) {
      openClasses = ' active';
    } else if(path.indexOf(extraPath) === 0 && path.length === extraPath.length) {
      extraClasses = ' active';
    }

    let overviewElement = overviewEnabled ?
      <a onClick={ overviewClick }>
        <div className={ overviewClasses }><i className={ navMenuIcons[0] }></i>{ Config.language.navMenu.overviewLabel }</div>
      </a> : null;

    let enrolledElement = enrolledEnabled ?
      <a onClick={ enrolledClick }>
        <div className={ enrolledClasses }><i className={ navMenuIcons[1] }></i>{ Config.language.navMenu.enrolledLabel }</div>
      </a> : null;

    let openElement = openEnabled ?
      <a onClick={ openClick }>
        <div className={ openClasses }><i className={ navMenuIcons[2] }></i>{ Config.language.navMenu.openLabel }</div>
      </a> : null;

    let extraElement = extraEnabled ?
      <a onClick={ extraClick }>
        <div className={ extraClasses }><i className={ navMenuIcons[3] }></i>{ Config.language.navMenu.extraLabel }</div>
      </a> : null;

    // work out how many tabs we have enabled by the Config
    let activeLength = _.filter([overviewEnabled, enrolledEnabled, openEnabled, extraEnabled], (check) => check).length - 1;
    // catch a 0 length array
    activeLength = activeLength < 0 ? 0 : activeLength;

    let accountFooter = activeLength > 0 && path !== '/' ?
      <CBox size='63px'>
        <div className={ 'navbar ui labeled icon fluid ' + navMenuOptions[activeLength] + ' ' + theme } style={{ marginTop:'0', borderRadius:'0' }}>
          { overviewElement }
          { enrolledElement }
          { openElement }
          { extraElement }
        </div>
      </CBox> :
      <CBox size='0px'>
      </CBox>;

    return (
      <Container>
        <VBox>
          <Panel>
            <RouterView router={ router }>
              <LandingPage path='/' />
              <IndexPage path='/plans/:planId' />
              <CourseListPage path='/plans/:planId/courses/:type' />
              <CoursePage path='/plans/:planId/courses/:type/course/:courseId' />
            </RouterView>
          </Panel>
          { accountFooter }
        </VBox>
      </Container>
    );
  }
});

reactRender(<Screen />);
